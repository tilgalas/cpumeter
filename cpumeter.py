import serial
import time

from collections import namedtuple

Stat = namedtuple('Stat', ("user", "nice", "system", "idle"))

def statSub(self, other):
    return Stat(self.user - other.user,
                self.nice - other.nice,
                self.system - other.system,
                self.idle - other.idle)

Stat.__sub__ = statSub


def readStat():
    with open('/proc/stat') as f:
        l = f.readline()
        while not l.startswith("cpu"):
            l = f.readline()

    field = l.split()
    return Stat(int(field[1]), int(field[2]), int(field[3]), int(field[4]))

def usage(stat):
    working = stat.user + stat.nice + stat.system
    return  working / (working + stat.idle)


def main():
    ser = serial.Serial(port='/dev/ttyACM0')
    s1 = readStat()
    while True:
        time.sleep(0.5)
        s2 = readStat()
        usageInt = int(round(usage(s2 - s1) * 255))
        ser.write(bytes([usageInt]))
        s1 = s2

if __name__ == "__main__":
    main()
